﻿using MarketStoreApplication.Abstract;
using MarketStoreApplication.Models;
using System;
using System.Text;

namespace MarketStoreApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            double purchaseValue;
            double calculatedDiscountRate;

            User ivanIvanov = new User("Ivan Ivanov");
            Card bronzeCard = new BronzeCard();
            ivanIvanov.Card = bronzeCard;
            bronzeCard.LastMonthTurnoverAmount = 0;
            calculatedDiscountRate = bronzeCard.CalculateCurrentDiscountRate();
            purchaseValue = 150;
            PrintCurrentPurchaseDetails(purchaseValue, calculatedDiscountRate);

            User yordanYordanov = new User("Yordan Yordanov");
            Card silverCard = new SilverCard();
            ivanIvanov.Card = silverCard;
            silverCard.LastMonthTurnoverAmount = 600;
            calculatedDiscountRate = silverCard.CalculateCurrentDiscountRate();
            purchaseValue = 850;
            PrintCurrentPurchaseDetails(purchaseValue, calculatedDiscountRate);

            User georgiGeorgiev = new User("Georgi Georgiev");
            Card goldCard = new GoldCard();
            georgiGeorgiev.Card = silverCard;
            goldCard.LastMonthTurnoverAmount = 1500;
            calculatedDiscountRate = goldCard.CalculateCurrentDiscountRate();
            purchaseValue = 1300;
            PrintCurrentPurchaseDetails(purchaseValue, calculatedDiscountRate);
        }

        static void PrintCurrentPurchaseDetails(double purchaseValue, double calculatedDiscountRate)
        {
            StringBuilder sb = new StringBuilder();

            double total = purchaseValue - (purchaseValue - (purchaseValue * calculatedDiscountRate));

            sb.AppendLine($"Purchase value: ${purchaseValue:f2}");
            sb.AppendLine($"Discount rate: {calculatedDiscountRate * 100:f1}%");
            sb.AppendLine($"Discount: ${total:f2}");
            sb.AppendLine($"Total: ${(purchaseValue - (purchaseValue * calculatedDiscountRate)):f2}");

            Console.WriteLine(sb.ToString());
        }
    }
}