﻿using MarketStoreApplication.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketStoreApplication.Models
{
    public class GoldCard : Card
    {
        private double _initialDiscountRate;

        public override double InitialDiscountRate
        {
            get => this._initialDiscountRate;
            set => value = 0.02;
        }

        public override double CalculateCurrentDiscountRate()
        {
            if (LastMonthTurnoverAmount >= 100 && LastMonthTurnoverAmount < 200)
            {
                return this.InitialDiscountRate += 0.03;
            }
            else if (LastMonthTurnoverAmount >= 200 && LastMonthTurnoverAmount < 300)
            {
                return this.InitialDiscountRate += 0.04;
            }
            else if (LastMonthTurnoverAmount >= 300 && LastMonthTurnoverAmount < 400)
            {
                return this.InitialDiscountRate += 0.05;
            }
            else if (LastMonthTurnoverAmount >= 400 && LastMonthTurnoverAmount < 500)
            {
                return this.InitialDiscountRate += 0.06;
            }
            else if (LastMonthTurnoverAmount >= 500 && LastMonthTurnoverAmount < 600)
            {
                return this.InitialDiscountRate += 0.07;
            }
            else if (LastMonthTurnoverAmount >= 600 && LastMonthTurnoverAmount < 700)
            {
                return this.InitialDiscountRate += 0.08;
            }
            else if (LastMonthTurnoverAmount >= 700 && LastMonthTurnoverAmount < 800)
            {
                return this.InitialDiscountRate += 0.09;
            }
            else if (LastMonthTurnoverAmount >= 800)
            {
                return this.InitialDiscountRate += 0.10;
            }

            return this.InitialDiscountRate = 0.02;
        }
    }
}
