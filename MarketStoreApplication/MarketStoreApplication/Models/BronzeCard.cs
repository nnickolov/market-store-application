﻿using MarketStoreApplication.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketStoreApplication.Models
{
    public class BronzeCard : Card
    {
        private double _initialDiscountRate;

        public override double InitialDiscountRate
        {
            get => this._initialDiscountRate;
            set => value = 1;
        }

        public override double CalculateCurrentDiscountRate()
        {
            if (this.LastMonthTurnoverAmount < 100)
            {
                this.InitialDiscountRate = 1;
                if (this.InitialDiscountRate == 1)
                {
                    this.InitialDiscountRate = 0;
                }
                return InitialDiscountRate;
            }
            else if (this.LastMonthTurnoverAmount >= 100 && this.LastMonthTurnoverAmount <= 300)
            {
                return 0.01;
            }
            else
            {
                return 0.025;
            }
        }
    }
}
