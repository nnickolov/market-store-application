﻿using MarketStoreApplication.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketStoreApplication.Abstract
{
    public abstract class Card
    {
        //Constructor
        public Card()
        {

        }

        //Properties
        public List<User> Users { get; set; }
        public double LastMonthTurnoverAmount { get; set; }
        public abstract double InitialDiscountRate { get; set; }

        public abstract double CalculateCurrentDiscountRate();
    }
}
