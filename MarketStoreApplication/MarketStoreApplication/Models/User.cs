﻿using MarketStoreApplication.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketStoreApplication.Models
{
    public class User
    {
        public User(string name)
        {
            this.Name = name;
        }
        public string Name { get; set; }
        public Card Card { get; set; }
    }
}
