﻿using MarketStoreApplication.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketStoreApplication.Models
{
    public class SilverCard : Card
    {
        private double _initialDiscountRate;

        public override double InitialDiscountRate
        {
            get => this._initialDiscountRate;
            set => value = 0.02;
        }

        public override double CalculateCurrentDiscountRate()
        {
            if (this.LastMonthTurnoverAmount > 300)
            {
                return 0.035;
            }
            else
            {
                return this.InitialDiscountRate = 0.02;
            }
        }
    }
}
